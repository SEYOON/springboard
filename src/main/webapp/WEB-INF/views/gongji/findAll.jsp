<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>findAll</title>
<!-- [2] CSS : 꾸미기  -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<!-- [3] JS : 움직임  -->
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<style type="text/css">
table {
	margin: auto;
	text-align: center;
	border: 0;
	cellspacing: 0;
	cellpadding: 5;
	width: 80%;
}

table th {
	background-color: rgba(0, 0, 0, .25);
	text-align: center;
}

h2 {
	text-align: center;
}

#width {
	width: 50%;
	border: 1px solid #444444;
}

input[type=text] {
	align: center;
	width: 40%;
	padding: 2px 2px;
}
</style>
</head>
<body>
	<h2>공지 테이블</h2>
	<br>
	<table class="table table-striped table-hover" id="width">
		<tr>
			<th width=100>id</th>
			<th width=200>제목</th>
		<%--<th width=100>작성자</th>  --%>
 			<th width=100 >작성자</th> 
			<th width=100>날짜</th>
		</tr>
		<c:forEach items="${dto.gongjis}" var="gongji">
			<tr>
				<td>${gongji.id }</td>
				<td><a href="oneView?id=${gongji.id}">${gongji.title}</a></td>
				<%--<td>${gongji.author}</td> --%>
				<td>${gongji.user.name }</td>
<%-- 				<td align=center>${gongji.user }</td> --%>
				<td><fmt:formatDate pattern="yyyy-MM-dd"
						value="${gongji.date}" /></td>
			</tr>
		</c:forEach>
	</table>
	<div>
		<form action="findAll" style="text-align: center;">
			<select name="searchOption" title="클릭하여 선택해주세요.">
				<option value="author"
					<c:out value="{$map.searchOption =='author'?'selected':''}"/>>작성자</option>
				<option value="title"
					<c:out value="{$map.searchOption =='title'?'selected':''}"/>>제목</option>
				<option value="content"
					<c:out value="{$map.searchOption =='content'?'selected':''}"/>>내용</option>
			</select> 
				<input type="text" name="keyword" value="${map.keyword}" placeholder="검색 내용을 입력해주세요." required> 
				<input type=submit value='검색'></input>
				<input type=button onClick=location.href='findAll'  value = '전체 리스트'></input>
		</form>
	</div>
	<c:if test="${empty searchOption}">
		<table>
			<tr>
				<td><a href="findAll?page=${dto.start}">Start</a></td>
				<td><a href="findAll?page=${dto.prev}">Prev</a></td>
				<c:forEach begin="${dto.firstPage}" end="${dto.lastPage}" step="1"
					var="i">
					<td><a href="findAll?page=${i}">${i}</a></td>
				</c:forEach>
				<td><a href="findAll?page=${dto.next}">Next</a></td>
				<td><a href="findAll?page=${dto.end}">End</a></td>
			</tr>
		</table>
	</c:if>
	<c:if test="${not empty searchOption}">
		<table>
			<tr>
				<td><a
					href="findAll?page=${dto.start}&searchOption=${searchOption}&keyword=${keyword}">Start</a></td>
				<td><a
					href="findAll?page=${dto.prev}&searchOption=${searchOption}&keyword=${keyword}">Prev</a></td>
				<c:forEach begin="${dto.firstPage}" end="${dto.lastPage}" step="1"
					var="i">
					<td><a
						href="findAll?page=${i}&searchOption=${searchOption}&keyword=${keyword}">${i}</a></td>
				</c:forEach>
				<td><a
					href="findAll?page=${dto.next}&searchOption=${searchOption}&keyword=${keyword}">Next</a></td>
				<td><a
					href="findAll?page=${dto.end}&searchOption=${searchOption}&keyword=${keyword}">End</a></td>
			</tr>
		</table>
	</c:if>
	<table>
		<tr>
			<td align=center><input type=button onClick="location=
				'create'" value='신규'></input></td>
		</tr>
	</table>

	<%-- 	last ${lastpage}
	total ${totalpage}
	count ${count}
	start ${startNum}
	endNum ${endNum}
	page ${page}
	next ${Next }
	Before${Before} --%>
</body>
</html>

