<%@ page contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>search</title>
</head>
<body>
	<table class="table table-striped table-hover" id="width">
		<tr>
			<th width=100 >id</th>
			<th width=200 >제목</th>
			<th width=100 >작성자</th>
			<th width=100 align=center>유저이름</th>
			<th width=100 >날짜</th>
		</tr>
	<c:forEach items="${gongji_list}" var="search">
		<tr>
			<td >${search.id }</td>
			<td ><a href="oneView?id=${search.id}">${search.title}</a></td>
			<td >${search.author }</td>
			 <td align=center>${search.user }</td> 
			<td ><fmt:formatDate pattern="yyyy-MM-dd" value="${search.date}"/></td>
		</tr>
	</c:forEach>
	</table>
</body>
</html>

