<%@ page contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>create</title>
<link href="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.css" rel="stylesheet">
<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script> 
<script src="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.js"></script> 

<!-- include summernote css/js-->
<link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.8/summernote.css" rel="stylesheet">
<script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.8/summernote.js"></script>
<script>
$(document).ready(function() {
     $('#summernote').summernote({
             height: 500,                 // set editor height
             minHeight: null,             // set minimum height of editor
             maxHeight: null,             // set maximum height of editor
             focus: true                  // set focus to editable area after initializing summernote
     });
});
$(document).ready(function() {
  $('#summernote').summernote();
});
</script>
<style type = "text/css">
  table {
	margin:auto;
    text-align: center;
    border: 5px;
    cellspacing: 0;
    cellpadding: 5;
    width: 80%;
  }
  table th {
 	background-color : rgba(0,0,0,.25);
  	text-align:center;
  	width : 30%;
  }

h2 {
   text-align:center;
}   

#width {
	width:60%;
}
input[type=text]{
	width: 100%;
	padding: 2px 2px;
}
</style>
</head>
<body>
<h2>신규 글 작성</h2>
<br>
<form action="write">
		<table class="table table-striped table-hover" id="width">
			<tr>
				<th>작성자</th>
				<td><input type="text" name= "author" value="" required></td>
			</tr>	
 			<tr>
				<th>유저이름</th>
				<td><input type="text" name= "user" value="${gongji.user }" required></td>
			</tr> 
			<tr>
				<th>제목</th>
				<td><input type="text" name= "title" value="" required></td>
			</tr>	
			<tr>
				<th>내용</th>
				<td><textarea id="summernote" type="text" style="resize:none" required name ="content" ></textarea></td>
			</tr>
			<tr>
				<th>날짜</th>
				<td><fmt:formatDate pattern="yyyy-MM-dd HH:mm" value="${date}"/></td>
			</tr>
		</table>	
		<table  align=center>
			<tr>
				<td><input type=button onClick=location.href='findAll' value = '취소'></input></td>
				<td><input type=submit  value='등록'></input></td> 
			</tr>
		</table>
</form>
</body>
</html>

