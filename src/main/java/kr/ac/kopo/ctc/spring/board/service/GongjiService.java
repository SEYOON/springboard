package kr.ac.kopo.ctc.spring.board.service;

import java.util.Date;
import java.util.List;

import kr.ac.kopo.ctc.spring.board.domain.Gongji;
import kr.ac.kopo.ctc.spring.board.dto.GongjiDto;

public interface GongjiService {
	/*
	 * List<Gongji>findAllByPage(int page); Page<Gongji> findAll(Pageable pageable);
	 * List<Gongji> findAll(int page, int cnt);
	 */
	List<Gongji> findAllByOrderByIdDesc(int page, int cnt);
	List<Gongji> findAll(int page, int cnt);
//	GongjiDto findByPage( int page, int cnt);
	GongjiDto findPage(int page, int cnt);
	void create(String title, String content, String author);
	Gongji findByOne(int id);
	Gongji update(String title, String content, int id, Date date);
	void delete (int id);

	/*
	 * List<Gongji> findAllSearchContent(String content, int page, int cnt);
	 * List<Gongji> findAllSearchTitle(String title, int page, int cnt);
	 * List<Gongji> findAllSearchAuthor(String author, int page, int cnt);
	 */
	//void update(String title, String content, int id, Date date);
	GongjiDto listAll(String searchOption, String keyword, int page, int cnt, int orderType);
	

}
