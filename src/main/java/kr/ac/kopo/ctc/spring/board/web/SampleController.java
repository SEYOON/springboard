package kr.ac.kopo.ctc.spring.board.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import kr.ac.kopo.ctc.spring.board.repository.SampleRepository;
import kr.ac.kopo.ctc.spring.board.service.SampleService;

@Controller
public class SampleController {

	private static final Logger logger = LoggerFactory.getLogger(SampleController.class);
	@Autowired
	private SampleRepository sampleRepository;
	
/*	@RequestMapping(value = "/sample/list")
	@ResponseBody
	public List<Sample>list(Model model) {
	//	return sampleRepository.findAll();
		return sampleRepository.findAllByTitleLike("%home%");
	//	return sampleRepository.findAllBylevelGreaterThan(5);
	}
*/
	@Autowired
	private SampleService  sampleService;
	@RequestMapping(value = "/sample/noAop")
	@ResponseBody
	public String noAop() {
		return sampleService.testNoAop();
	}
	
	@RequestMapping(value="/sample/aop")
	@ResponseBody
	public String aop() {
		return sampleService.testAop();
	}
	
	
	
	
	
	
	/*
	 * @RequestMapping(value="/sample/noTranscactional")
	 * 
	 * @ResponseBody public String noTransactional() { return
	 * sampleService.testNoTransactional(); }
	 * 
	 * @RequestMapping(value="/sample/transcactional")
	 * 
	 * @ResponseBody public String transactional() { return
	 * sampleService.testTransactional(); }
	 */
}
