package kr.ac.kopo.ctc.spring.board.web;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import kr.ac.kopo.ctc.spring.board.domain.Sample;

@Controller
public class HelloController {
	
	@RequestMapping(value="/hello2")
	public String helloSpringBoot(Model model) {
		
		model.addAttribute("name", "홍길동");
		model.addAttribute("age", "30");
		model.addAttribute("tel", "010-111-1111");
		model.addAttribute("email", "hkg@naver.com");
		
		Sample s = new Sample();
		s.setId(5L);
		s.setTitle("title1234");
		model.addAttribute("sample", s);
		
		return "hello2";
	}

}
