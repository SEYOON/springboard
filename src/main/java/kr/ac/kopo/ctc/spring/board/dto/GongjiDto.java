package kr.ac.kopo.ctc.spring.board.dto;

import java.util.List;

import kr.ac.kopo.ctc.spring.board.domain.Gongji;

public class GongjiDto {
	List <Gongji> gongjis;
	private int firstPage;
	private int lastPage;
	private int totalpage;
	private int prev;
	private int next;
	private int curr;
	private int start;
	private int end;
	public int getStart() {
		return start;
	}
	public void setStart(int start) {
		this.start = start;
	}
	public int getEnd() {
		return end;
	}
	public void setEnd(int end) {
		this.end = end;
	}
	public List<Gongji> getGongjis() {
		return gongjis;
	}
	public void setGongjis(List<Gongji> gongjis) {
		this.gongjis = gongjis;
	}
	public int getFirstPage() {
		return firstPage;
	}
	public void setFirstPage(int firstPage) {
		this.firstPage = firstPage;
	}
	public int getLastPage() {
		return lastPage;
	}
	public void setLastPage(int lastPage) {
		this.lastPage = lastPage;
	}
	public int getTotalpage() {
		return totalpage;
	}
	public void setTotalpage(int totalpage) {
		this.totalpage = totalpage;
	}
	public int getPrev() {
		return prev;
	}
	public void setPrev(int prev) {
		this.prev = prev;
	}
	public int getNext() {
		return next;
	}
	public void setNext(int next) {
		this.next = next;
	}
	public int getCurr() {
		return curr;
	}
	public void setCurr(int curr) {
		this.curr = curr;
	}

	
		
	/*
	 * public interface Page<T> extends Iterable<T> {
	 * 
	 * 
	 * 
	 * int getNumber(); //현재 페이지
	 * 
	 * int getSize(); //페이지 크기
	 * 
	 * int getTotalPages(); //전체 페이지 수
	 * 
	 * int getNumberOfElements(); //현재 페이지에 나올 데이터 수
	 * 
	 * long getTotalElements(); //전체 데이터 수
	 * 
	 * boolean hasPreviousPage(); //이전 페이지 여부
	 * 
	 * boolean isFirstPage(); //현재 페이지가 첫 페이지 인지 여부
	 * 
	 * boolean hasNextPage(); //다음 페이지 여부
	 * 
	 * boolean isLastPage(); //현재 페이지가 마지막 페이지 인지 여부
	 * 
	 * Pageable nextPageable(); //다음 페이지 객체, 다음 페이지가 없으면 null
	 * 
	 * Pageable previousPageable();//다음 페이지 객체, 이전 페이지가 없으면 null
	 * 
	 * List<T> getContent(); //조회된 데이터
	 * 
	 * boolean hasContent(); //조회된 데이터 존재 여부
	 * 
	 * Sort getSort(); //정렬 정보
	 * 
	 * 
	 * 
	 * }
	 */
		
		
}
