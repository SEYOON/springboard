package kr.ac.kopo.ctc.spring.board.aop;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class LogAspect {
		private static final Logger Logger = LoggerFactory.getLogger(LogAspect.class);
		
		@Before("execution(* kr.ac.kopo.ctc.spring.board.service.*.*Aop(..))")
		public void onBeforeHandler (JoinPoint joinPoint) {
			Logger.info("=================== onBeforeThing");
		}
		@After("execution(* kr.ac.kopo.ctc.spring.board.service.*.*Aop(..))")
		public void onAfterHandler(JoinPoint joinPoint) {
			Logger.info("=================== onAfterHandler");
		}
		@Around("execution(* kr.ac.kopo.ctc.spring.board.service.*.*Aop(..))")
		public Object onAround(ProceedingJoinPoint proceedingJoinPoint) {
			Logger.info("@Around start");
			Object result = null;
			try {
				result = proceedingJoinPoint.proceed();
			}catch(Throwable e) {
				e.printStackTrace();
			}
			Logger.info("@Around End");
			return result;
		}
	/*
	 * @AfterReturning(pointcut = "onPointcut()") public void
	 * onAfterReturningByPointcut() {
	 * Logger.info("=================== onAfterReturningHandler"); }
	 */
	   @AfterReturning(pointcut =
	    "execution(* kr.ac.kopo.ctc.spring.board.service.*.*Aop(..))", returning =
	    "str") public void onAfterReturningHandler(JoinPoint joinPoint, Object str) {
	    Logger.info("@AfterReturning : " + str);
	    Logger.info("=================== onAfterReturningHandler"); }
		@Pointcut("execution(* kr.ac.kopo.ctc.spring.board.service.*.*Aop(..))")
		public void onPointcut(JoinPoint joinPoint) {
			Logger.info("=================== onPointcut");
		}
}
