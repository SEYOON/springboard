package kr.ac.kopo.ctc.spring.board.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import kr.ac.kopo.ctc.spring.board.domain.Gongji;
import kr.ac.kopo.ctc.spring.board.domain.User;

@Repository
public interface GongjiRepository extends JpaRepository<Gongji, Integer> {
	List<Gongji> findAllById(int i);
	Page<Gongji> findAll(Pageable pageable);
	Page<Gongji> findAllByOrderByIdDesc(Pageable pageable);
	Page<Gongji> findAllByAuthor(String author, Pageable pageable);
					// 아래 Gongji는 테이블 gongji가 아니다. 클래스파일 Gongji를 말한다 
	@Query("select t from Gongji t where content like concat('%', :searchString , '%')")
	Page<Gongji> findAllSearch(@Param("searchString") String searchString, Pageable pageable);
	List<Gongji> findAll();
	Page<Gongji> findAllByUser(User user, Pageable pageable);
	   
	@Query("select g from Gongji g where title like concat('%', :keyword, '%')")
	Page<Gongji> findAllSearchTitle(@Param("keyword") String keyword, Pageable pageable);
	
//	Page<Gongji> findAllByTitleDesc(String title, Pageable pageable);
//	Page<Gongji> findAllByTitleAsc(String title, Pageable pageable);
	   
    @Query("select t from Gongji t where author like concat('%', :keyword, '%')")
	Page<Gongji> findAllSearchAuthor(@Param("keyword") String keyword, Pageable pageable);
	   
	@Query("select t from Gongji t where content like concat('%', :keyword, '%')")
    Page<Gongji> findAllSearchContent(@Param("keyword") String keyword, Pageable pageable);

	
}
//	List<Gongji> findAll(int page, int cnt);
//List<Gongji> findAllByOrderByIdDesc(int page, int cnt);
////GongjiDto findByPage( int page, int cnt);

//GongjiDto findPage(int page, int cnt);

/*
 * @Modifying
 * 
 * @Transactional
 * 
 * @Query("update gongji set title = :title, content= :content where id = :id")
 * void update (@Param("title")String title, @Param("content") String
 * content, @Param("id") int id);
 */
