package kr.ac.kopo.ctc.spring.board.web;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import kr.ac.kopo.ctc.spring.board.domain.Gongji;
import kr.ac.kopo.ctc.spring.board.dto.GongjiDto;
import kr.ac.kopo.ctc.spring.board.repository.GongjiRepository;
import kr.ac.kopo.ctc.spring.board.service.GongjiService;


enum ORDER_TYPE {
	ASC(0),
	DESC(1);
	private int value;
	  private ORDER_TYPE(int value) {
	    value = value;
	  }
	  public int getValue() {
	    return value;
	  }
}


@Controller
public class GongjiController {

	@Autowired
	private GongjiRepository gongjiRepository;
	// 위에는 임시, 나중에는 서비스에 의존하도록 바꿔야한다.
	@Autowired
	private GongjiService gongjiService;

	/*
	 * @RequestMapping(value="/gongji/findAll") public String findAll(Model
	 * model, @RequestParam HashMap<String, String>map) { int page = 0; // 현재 페이지
	 * int cnt = 10; // 보여 줄 게시글 수 int pageNum = 10; // 보여 줄 번호 수 String pages =
	 * map.get("page"); if(pages == null) { pages = "1"; } page =
	 * Integer.parseInt(pages); int lastpage = gongjiService.findPage(page,
	 * cnt).getLastPage(); // 리스트 총 개수 Long totalpage =gongjiService.findPage(page,
	 * cnt).getTotalpage(); int startNum = ((page -1)/pageNum) * pageNum + 1; int
	 * endNum = startNum + 9; if (endNum > lastpage) { endNum = lastpage; } int
	 * Before = startNum -10; if(Before < 0 ) { Before = 1; } Long Next = (long)
	 * (startNum + 10); if(Next > lastpage) { Next = (long) lastpage; } List<Gongji>
	 * gongji_list = gongjiService.findAllByOrderByIdDesc(page-1, cnt); int count =
	 * (int) gongjiRepository.count(); model.addAttribute("count", count);
	 * model.addAttribute("gongji_list", gongji_list);
	 * model.addAttribute("startNum", startNum); model.addAttribute("endNum",
	 * endNum); model.addAttribute("lastpage", lastpage);
	 * model.addAttribute("totalpage", totalpage); model.addAttribute("page", page);
	 * model.addAttribute("Before", Before); model.addAttribute("Next", Next); //
	 * model.addAttribute("viewpage", viewpage); String id = map.get("id"); String
	 * author = map.get("author"); String date = map.get("date"); String title =
	 * map.get("title"); String content = map.get("content"); String pagess =
	 * map.get("page"); if (pagess == null) { pagess = "1"; }
	 * 
	 * String keyword = map.get("keyword"); String searchOption =
	 * map.get("searchOption"); int pagei = Integer.parseInt(pagess); if (keyword !=
	 * null) { GongjiDto dto = gongjiService.listAll(searchOption, keyword, 0);
	 * model.addAttribute("dto", dto); } else { GongjiDto dto =
	 * gongjiService.findPage(Integer.parseInt(pagess), 10);
	 * model.addAttribute("dto", dto); } return "gongji/findAll"; // // //GongjiDto
	 * gongji_list = gongjiService.findByPage(page, cnt); // List<Gongji>gongji_list
	 * = gongjiService.findAll(page -1 , cnt); // //List<Gongji>gongji_list =
	 * gongjiRepository.findAll();
	 * 
	 * GongjiDto dto = gongjiService.findAll(page -1 , cnt);
	 * model.addAttribute("dto", dto);
	 * 
	 * 
	 * }
	 */
	//findAll : 전체 리스트 보기
	@RequestMapping(value="/gongji/findAll")
	public String findAll(Model model, @RequestParam HashMap<String, String> map) { 
	
		
		String page = map.get("page");
		if (page == null) {
			page = "1";
		}
			
		String keyword = map.get("keyword");
		String searchOption = map.get("searchOption");
		int pagei = Integer.parseInt(page);
		if (keyword != null) {
			GongjiDto dto = gongjiService.listAll(searchOption, keyword, pagei-1, 10, ORDER_TYPE.ASC.getValue());
			model.addAttribute("keyword",keyword);
			model.addAttribute("searchOption",searchOption);
			model.addAttribute("dto", dto);
		} else {
			GongjiDto dto = gongjiService.findPage(Integer.parseInt(page)-1, 10);
			
			model.addAttribute("dto", dto);
		}
		
		return "gongji/findAll";
	}
	//검색
	@RequestMapping("value=gongji/findAll")
	public ModelAndView list(@RequestParam(defaultValue="title") String searchOption,
			@RequestParam(defaultValue="") String keyword, @RequestParam(defaultValue="0") int page) {
		
		GongjiDto dto = gongjiService.listAll(searchOption, keyword, 10, 10, ORDER_TYPE.ASC.getValue());
		ModelAndView mav = new ModelAndView();
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("searchOption", searchOption);
		map.put("keyword", keyword);
		map.put("searchOption", searchOption);		
		mav.addObject("map", map);
		
		return mav;
	}

	@RequestMapping(value = "/gongji/oneView")
	public String oneView(Model model, @RequestParam HashMap<String, String> map) {
		int id = Integer.parseInt(map.get("id"));
		List<Gongji> gongji_list = gongjiRepository.findAllById(id);
		model.addAttribute("gongji_list", gongji_list);
		return "gongji/oneView";
	}

	@RequestMapping(value = "/gongji/create")
	public String create(Model model, @RequestParam HashMap<String, String> map) {
		Date date = new Date();
		model.addAttribute("date", date);
		return "gongji/create";
	}

	@RequestMapping(value = "/gongji/write")
	public String write(Model model, @RequestParam HashMap<String, String> map) {
		String title = map.get("title");
		String content = map.get("content");
		String author = map.get("author");
		String ids = map.get("id");
		if (ids == null) {
			gongjiService.create(title, content, author);
		} else {
			int id = Integer.parseInt(ids);
			Date date = new Date();
			gongjiService.update(title, content, id, date);
		}
		return "gongji/write";
	}

	@RequestMapping(value = "/gongji/update")
	public String update(Model model, @RequestParam HashMap<String, String> map) {
		String ids = map.get("id");
		int id = Integer.parseInt(ids);
		Date date = new Date();
		Gongji gongji = gongjiService.findByOne(id);
		model.addAttribute("gongji", gongji);
		model.addAttribute("id", id);
		model.addAttribute("date", date);
		return "gongji/update";
	}

	@RequestMapping(value = "/gongji/delete")
	public String delete(Model model, @RequestParam HashMap<String, String> map) {
		String ids = map.get("id");
		int id = Integer.parseInt(ids);
		gongjiService.delete(id);
		// model.addAttribute("id", id);
		return "redirect:findAll";
	}

	@RequestMapping(value = "/api/gongji/findAll")
	public String apiFindAll(Model model, @RequestParam HashMap<String, String> map) {
		model.addAttribute("name", "홍길동2");
		return "api/gongji/findAll";
	}

}
