package kr.ac.kopo.ctc.spring.board.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import kr.ac.kopo.ctc.spring.board.domain.Member;

@Repository
public interface MemberRepository extends JpaRepository<Member,Integer>{ 
	// 도메인만 잡아주면 알아서  해준다.
	List<Member>findByNameAndAgeLessThan(String name, int age);
	
}
