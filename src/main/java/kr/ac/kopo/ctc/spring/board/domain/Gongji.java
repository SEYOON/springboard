package kr.ac.kopo.ctc.spring.board.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Gongji {
	@Id
	@GeneratedValue
	@Column	
	private Integer id;
	
	@Column
	private String title;
	
	@Column
	private Date date;
	
	@Column
	private String content;
	
	@Column
	private String author;
	

	@ManyToOne(optional=false)
	@JoinColumn(name="user_id")
	private User user;

	public Gongji() {
	// 커스텀한 생성자가 있으면 기본 생성자가 생성되지 않는다
	}// 기본 생성자를 사용하려고 하는데 없기 때문에 에러 발생, 반드시 써줘야함 
	
	public Gongji(String title, String content) {
		this.title = title;
		this.content =content;
	}
	public Gongji(String title, String content, Date date) {
		this.title = title;
		this.content = content;
		this.date = date;
	}
	public Gongji(String title, String content, String author, Date date) {
		this.title = title;
		this.content = content;
		this.author =author;
		this.date = date;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	@Override
	public String toString() {
		String result = "[gongji_"+id+"]" + title;
		return result;
	}	
}
