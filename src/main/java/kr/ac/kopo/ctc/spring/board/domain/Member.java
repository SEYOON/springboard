package kr.ac.kopo.ctc.spring.board.domain;
import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Member {
	@Id
	@GeneratedValue
	@Column
	private Integer id;
	
	@Column
	private String name;
	
	@Column 
	private int age;
	
	public Member(String name) {
	      this.name = name;
	}
	 // 생성자 추가 
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	// cascadeType 
	// phone에서는 member가 나오는데 phone에서는 member가 나오지 않는다. member가 원천기술인 셈 
	@OneToMany(cascade=CascadeType.ALL, mappedBy="member")
	private Collection<Phone> phones;
	// 편하게 쓰려고 추가한 것. Phones에 대한 get, set
	public Collection<Phone> getPhones() {	
		if(phones == null) {
			phones = new ArrayList<Phone>();
		} // 한번 null 체크를 해야한다. null이 아닌 경우에 add를 해야한다. 
		//  null check는 누구에게 책임이 있는가 ?
		// web? service? dao? domain? db? 응집성(같은 성격의 코드) 로 보면  domain이 적당하지 않을까? (라고 교수님 생각)
		// 응집성 있게 프로그램 짤때 적당한 위치에 넣으면 된다. 
		return phones;
	}
	
	public void setPhones(Collection<Phone> phones) {
		this.phones = phones;
	}
	
	public void addPhone(Phone p) {
		Collection<Phone> phones = getPhones();
		p.setMember(this);
		phones.add(p);
	}
	@Override
	public String toString() {
		String result = "["+id+"]" + name;
		for(Phone p : getPhones()) {
			result += "\n" + p.toString();
		}
		return result;
	}
}
