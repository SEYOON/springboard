package kr.ac.kopo.ctc.spring.board.domain;

import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class User {
	@Id
	@GeneratedValue
	@Column	
	private Integer id;
	
	@Column
	private String name;
	
	@Column
	private int age;
	// cascadeType 
	// user에서는 gongji가 나오는데 gongji에서는 user가 나오지 않는다. user가 원천기술인 셈 
	@OneToMany(cascade=CascadeType.ALL, mappedBy="user")
	private Collection<Gongji> gongjis;
	public User() {	
	}
    public User(int id) {
		this.id=id;
	}
	public User(String name) {
		this.name = name;
	}
	public User(String name, int age) {
		this.name = name;
		this.age = age;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	 //Array List에 테이블 내용 넣기

	public Collection<Gongji> getGongjis() {
		if(gongjis == null) {
			gongjis = new ArrayList<Gongji>();
		}
		return gongjis;
	}
	
	public void setgongjis(Collection<Gongji> gongjis) {
		this.gongjis = gongjis;
	}
	
	public void addGongji(Gongji n) {
		Collection<Gongji> gongjis = getGongjis();
		n.setUser(this);
		gongjis.add(n);
	}
	
	@Override
	public String toString() {
//		String result = "[" + id + "]" + name;
//		for(Gongji n : getGongjis()) {
//			result += "\n" + n.toString();
//		}
//		return result;
		return this.name;
	}

}
