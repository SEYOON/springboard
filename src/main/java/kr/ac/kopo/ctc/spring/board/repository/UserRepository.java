package kr.ac.kopo.ctc.spring.board.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import kr.ac.kopo.ctc.spring.board.domain.User;

@Repository
public interface UserRepository extends JpaRepository<User, Integer>{
	User findByName(String name);
//	 List<User> findByName(String name);
}
