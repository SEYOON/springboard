package kr.ac.kopo.ctc.spring.board.service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import kr.ac.kopo.ctc.spring.board.domain.Gongji;
import kr.ac.kopo.ctc.spring.board.domain.User;
import kr.ac.kopo.ctc.spring.board.dto.GongjiDto;
import kr.ac.kopo.ctc.spring.board.repository.GongjiRepository;
import kr.ac.kopo.ctc.spring.board.repository.UserRepository;

@Service
public class GongjiServiceImpl implements GongjiService{

	@Autowired
	private GongjiRepository gongjiRepository;
	
	@Autowired
	private UserRepository userRepository;

	@Override
	public List<Gongji> findAll(int page, int cnt) {
		return gongjiRepository.findAll(PageRequest.of(page,  cnt)).getContent();
	}
	
	int firstPage = 0;
	int next = 0;
	int prev = 0;
	int block = 10;
	int lastPage =0;
	//페이지네이
	private GongjiDto pagination(int page, int cnt, Page<Gongji> gongji_list) {
		if (page > 0) {
			page = page-1;
			firstPage = ((page+1)/block)*block+1;	// sblock
		} else {
			firstPage=1;
		}

		int totalPage = gongji_list.getTotalPages();

		GongjiDto gongjiDto = new GongjiDto();

		 lastPage = firstPage + 9;
	      if(lastPage>totalPage) {
	    	  lastPage = totalPage;
	      }
		
		if (page<firstPage+1) {// before
			prev = 1;
		} else {
			prev = firstPage-10;
		}
		
		next = firstPage + 10;
		if (next > totalPage) { // next
			next= totalPage;
		}

		gongjiDto.setGongjis(gongji_list.getContent());
		gongjiDto.setStart(1);
		gongjiDto.setFirstPage(firstPage);
		gongjiDto.setLastPage(lastPage);
		gongjiDto.setEnd(totalPage);
		gongjiDto.setTotalpage(totalPage);
		
		gongjiDto.setPrev(prev);
		gongjiDto.setNext(next);
		
		return gongjiDto;
	}
	
	@Override
	public GongjiDto findPage(int page, int cnt) {
//		Page<Gongji> gongji_list = gongjiRepository.findAll(PageRequest.of(page, cnt));
		Page<Gongji> gongji_list = gongjiRepository.findAllByOrderByIdDesc(PageRequest.of(page, cnt));
		return pagination(page, cnt, gongji_list);
	}


	@Override
	public List<Gongji> findAllByOrderByIdDesc(int page, int cnt) {
		return gongjiRepository.findAllByOrderByIdDesc(PageRequest.of(page, cnt)).getContent();
	}

	@Override
	public void delete(int id) {
		gongjiRepository.deleteById(id);	
	}

	@Override
	public void create(String title, String content, String author) {
		Gongji g = new Gongji();
		
		User u = userRepository.findByName(author);
		if(u == null) {
			u = new User(author);
			userRepository.save(u);
		}
		g.setUser(u);
		g.setTitle(title);
		g.setContent(content);
		g.setDate(new Date());
		g.setAuthor(author);
		gongjiRepository.save(g);
		
	}

	@Override
	public Gongji update(String title, String content, int id, Date date) {
		Gongji g = gongjiRepository.findById(id).get();
		g.setTitle(title);
		g.setContent(content);
		g.setDate(date);
		return gongjiRepository.save(g);
	}

	@Override
	public Gongji findByOne(int id) {
		
		return gongjiRepository.findById(id).get();
	}

	@Override
	public GongjiDto listAll(String searchOption, String keyword, int page, int cnt, int orderType) { // orderType: 0(ASC), orderType: 1(DESC)
		Page<Gongji> gongji_list = null;
		if (searchOption.equals("title")) {
			System.out.println("1");
			gongji_list = gongjiRepository.findAllSearchTitle(keyword, PageRequest.of(page, cnt));
			
//			if (orderType == 0) {
//				gongji_list = gongjiRepository.findAllByTitleAsc(keyword, PageRequest.of(page, cnt));
//			} else {
//				gongji_list = gongjiRepository.findAllByTitleDesc(keyword, PageRequest.of(page, cnt));
//			}
			
		} else if (searchOption.equals("author")) {
			System.out.println("2");
			//String author = gongjiRepository.findByAuthor(keyword);
//			String name = userRepository.findByName(keyword).getName();
//			if(name == null) {
//				gongji_list = gongjiRepository.findAllByOrderByIdDesc(PageRequest.of(page, cnt));
//			}else {
//				gongji_list = gongjiRepository.findAllSearchAuthor(keyword, PageRequest.of(page, cnt));
//			}
			User u = userRepository.findByName(keyword);
			gongji_list = gongjiRepository.findAllByUser(u, PageRequest.of(page, cnt));
//			gongji_list = gongjiRepository.findAllSearchAuthor(keyword, PageRequest.of(page, cnt));
		} else if (searchOption.equals("content")) {
			System.out.println("3");
			gongji_list = gongjiRepository.findAllSearchContent(keyword, PageRequest.of(page, cnt));

			
		} else {
			System.out.println("4");
			gongji_list = Page.empty();
			
//			return gongjiDto;
		}
		
		
		return pagination(page, cnt, gongji_list);
		
		
		
	}




}



/*
 * private static final Logger logger =
 * LoggerFactory.getLogger(GongjiServiceImpl.class);
 * 
 * @Autowired private GongjiRepository gongjiRepository;
 * 
 * public List<Gongji> findAllByPage(int page) { Page<Gongji> pages =
 * gongjiRepository.findAll(PageRequest.of(page,10)); return pages.getContent();
 * }
 * 
 * 
 * @Override public int getMaxPage(int cnt) { Page<Gongji> pages =
 * gongjiRepository.findAll(PageRequest.of(0,cnt)); int maxPage =
 * pages.getTotalPages(); return maxPage; }
 * 
 * @Override public List<Gongji> findAll(Pageable pageable) { return
 * gongjiRepository.findAll().getContent(); }
 * 
 * 
 * @Override public List<Gongji> findAll(int page, int cnt) { return
 * gongjiRepository.findAll(PageRequest.of(page, cnt)).getContent(); }
 */
/*
 * @Override public GongjiDto findByPage(int page, int cnt) {
 * 
 * Page<Gongji> gongji_list = gongjiRepository.findAll(PageRequest.of(page,
 * cnt)); GongjiDto gongjiDto = new GongjiDto(); //
 * gongjiDto.setGongjis(gongji_list.getContent()); // gongjiDto.setFirst(0); //
 * gongjiDto.setLast(10); // gongjiDto.setCurrent(5); return gongjiDto;
 * 
 * }
 */