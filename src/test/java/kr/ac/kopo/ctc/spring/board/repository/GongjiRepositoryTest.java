package kr.ac.kopo.ctc.spring.board.repository;

import javax.transaction.Transactional;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.test.context.junit4.SpringRunner;

import kr.ac.kopo.ctc.spring.board.domain.Gongji;
import kr.ac.kopo.ctc.spring.board.domain.User;



@RunWith(SpringRunner.class)
@SpringBootTest
public class GongjiRepositoryTest {
	@Autowired
	private GongjiRepository gongjiRepository;

	@Autowired
	private UserRepository userRepository;
	// user_id를 위해 추가 
	
	
	
	
	@Ignore
	@Test
	@Transactional
	public void oneToMany_OneWay() {
		// user_id 가 null값이기 때문에 id값을 가져온다 
		User u =userRepository.findById(758).get();
		// 미리 아이템 100개를 적재/	
		for(int i = 0 ; i < 100; i++){
		Gongji g = new Gongji();
			g.setContent(dummyString());
		if ( i % 3 == 0 ){
			g.setAuthor("first");
		} else if( i % 3 == 1 ) {
			g.setAuthor("second");
		} else {
			g.setAuthor("third");
		}
		g.setUser(u); // 가져온 값을 넣어준다 
		gongjiRepository.save(g);
	}
		
	

		// 한 페이지 아이템 10개, 0번째 페이지 호출
		Page<Gongji> page = gongjiRepository.findAll(PageRequest.of(0, 10));
		printPageData("simple", page);
	
		// 한 페이지 아이템 10개, 글번호 내림차순으로, 0번째 페이지 호출
		page = gongjiRepository.findAllByOrderByIdDesc(PageRequest.of(0, 10));
		printPageData("sort_seq_desc", page);
	
		// 한 페이지에 아이템 10개, 글번호 내림차순으로, 2번째 페이지 호출
		page = gongjiRepository.findAll(PageRequest.of(2, 10, new Sort(Direction.DESC, "id")));
		printPageData("sort", page);
	
		// 한페이지에 아이템 10개, 글작성자 "first", 0번째 페이지 호출
		page = gongjiRepository.findAllByAuthor("first", PageRequest.of(0, 10));
		printPageData("sort_author", page);
	
		// 한페이지 아이템 10개, 작성자 내림차순으로, 2번째 페이지 호출
		page = gongjiRepository.findAll(PageRequest.of(2, 10, new Sort(Direction.DESC, "author")));
		printPageData("sort_author_desc", page);
	
		// 한페이지 아이템 10개, 검색어 중, 글번호 내림차순으로, 2번째 페이지 호출
		page = gongjiRepository.findAllSearch("bc", PageRequest.of(2, 10, new Sort(Direction.DESC, "id")));
		printPageData("sort_search_desc", page);
	}

	// 페이지 데이터 출력
	private void printPageData(String label, Page<Gongji> page){
		if( page == null || page.getSize() <= 0 ) 
			return;
	
		for( int i = 0 ; i < page.getSize(); i++ ){
			Gongji g = page.getContent().get(i);
			System.out.println("["+label+"] "+ g.getId() + " " + g.getTitle() + " " + g.getContent());
		}
	}

	// 더미 문자열 반환
	private String dummyString(){
		String [] dummy = {"abc", "bcd", "cde", "def"};
		int rand = (int)(System.currentTimeMillis() % dummy.length);
		return dummy[rand];
	}
	
	
	

}
