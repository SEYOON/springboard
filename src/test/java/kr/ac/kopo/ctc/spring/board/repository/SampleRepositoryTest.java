package kr.ac.kopo.ctc.spring.board.repository;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SampleRepositoryTest {

	@Autowired // 자동으로 연결 (DI)
	private SampleRepository sampleRepository;
	
	
	@Before
	public void before() {
		System.out.println("before");
	} // 밑에 본문이 실행 되기 전에 먼저 실행
	@After
	public void after() {
		System.out.println("after");
	} // 밑에 본문이 실행 된 후에 실행
	// 실행 된 값을 보기 편하게 해준다. 
	
//	@Test
//	public void findAll() {
//		assertEquals(7,sampleRepository.findAll().size());
//	}
	
//	@Test
//	public void equalTest() {
//		assertEquals("a","a");
//	}
	
	@Test
	@Ignore
	public void notEqualTest() {
		assertNotEquals("a","b");
	}

}
