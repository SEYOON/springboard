package kr.ac.kopo.ctc.spring.board.repository;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import kr.ac.kopo.ctc.spring.board.domain.Gongji;
import kr.ac.kopo.ctc.spring.board.domain.User;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserRepositoryTest {
	
	@Autowired
	private UserRepository userRepository;
	@Ignore
	@Test
	@Transactional
	public void oneToMany_TwoWay() {
		
		User first = new User("Jung", 20);
		first.addGongji(new Gongji("title1", "1", "Jung", new Date()));
		first.addGongji(new Gongji("title2", "22", "Jung", new Date()));
		
		User second = new User("Dong", 30);
		second.addGongji(new Gongji("title3","333", "Dong", new Date()));
		
		userRepository.save(first);
		userRepository.save(second);
		
		List<User> list = userRepository.findAll();
		
		for( User u : list) {
			System.out.println(u.toString());
		}
		userRepository.deleteAll();
	}
	
	@Test
	public void test() {
		String name = userRepository.findByName("1111").getName();
//		if(u == null) {
//			u = new User("1111");
//			userRepository.save(u);
//		}
		System.out.println(name);
//		
//		
//		String result = userRepository.findByName("762").getName();
//		System.out.println(result);
	}
}
